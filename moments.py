import wave
import audioop

import math
from scipy.fftpack import fft
import numpy as np
import matplotlib.pyplot as plt


class WavFile:
		def __init__(self, filename, dureeLecture, buffer = 4096):
				self.filename = filename
				self.file = None
				self.buffer = buffer
				self.dureeLecture = dureeLecture

		def __enter__(self):
				self.file = wave.open(self.filename, "rb").__enter__()
				(self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
				# assert self.nchannels == 1, "Only mono wave files are supported"
				self.length = self.nsamples/self.samplerate
				self.max_amplitude = 1<<(8*self.samplewidth -1)
				return self

		def __exit__(self, *args, **kwargs):
				if self.file is not None:
						self.file.__exit__(*args, **kwargs)
						self.file = None

		def get_samples(self):
			frames = self.file.readframes((int)(self.samplerate * self.dureeLecture))
			for index in range(len(frames)//self.samplewidth):
				sample = audioop.getsample(frames, self.samplewidth, index)
				yield sample/self.max_amplitude

						
#with WavFile("D:\PSC\Echantillons\Elise (1).wav", 0.1) as fichier:
with WavFile("D:\PSC\Echantillons\elise (2).wav"", 5) as fichier:
	
	a = list(fichier.get_samples())
	print("fichier ouvert")
	fs = fichier.samplerate
	
	d = len(a)

	### On va découper en fenêtres de 0.1 secondes ###

	fenetre = 0.1
	tailleFenetre = fenetre * fs

	nombreFenetres = round(d / tailleFenetre) #ou floor ?

	listeFenetres = list(range(0,nombreFenetres))

	#### Extraction des sous-listes ####
	for i in range(0, nombreFenetres):
		listeFenetres[i] = a[tailleFenetre * i : tailleFenetre*(i+1)-1]   ### Extraction de la sous-liste ###

	for i in range(0, nombreFenetres):
		morceauSpectre[i] = fft(listeFenetres[i])

	### extraction des moments musicaux ###

	moment1 = list(range(0, nombreFenetres))
	moment2 = list(range(0, nombreFenetres))

	for k in range(0, nombreFenetres):
		moment1[k] = 0
		sumTemp = 0
		local = k * tailleFenetre
		for i in range(0, tailleFenetre):
			moment1[k] += morceauSpectre[k][i] * i
			moment2[k] += morceauSpectre[k][i] * morceauSpectre[k][i] * i
			sumTemp += i
		moment1[k] /= sumTemp ## Moyenne d'ordre 1 du spectre sur la fenêtre ##
		moment2[k] /= sumTemp

	largeurSpectrale = list(range(0, nombreFenetres))
	for k in range(0, nombreFenetres):
		largeurSpectrale[k] = sqrt(moment2[k] - (moment1[k]*moment1[k]))

	#### AFFICHAGE ####
	# plt.plot(abs(c[0:(d - 1)]), 'r')

	plt.plot(moment1[0:nombreFenetres], 'r')
	plt.show()

	
