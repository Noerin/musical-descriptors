# -*- coding:Utf-8 -*-

import wave
import audioop

import math
from scipy.fftpack import fft
import numpy as np
import matplotlib.pyplot as plt


class WavFile:
		def __init__(self, filename, fenetre):
				self.filename = filename
				self.file = None
				self.dureeLecture = fenetre

		def __enter__(self):
				self.file = wave.open(self.filename, "rb").__enter__()
				(self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
				# assert self.nchannels == 1, "Only mono wave files are supported"
				self.length = self.nsamples/self.samplerate
				self.max_amplitude = 1<<(8*self.samplewidth -1)
				return self

		def __exit__(self, *args, **kwargs):
				if self.file is not None:
						self.file.__exit__(*args, **kwargs)
						self.file = None
						
		def get_samples(self):
			frames = self.file.readframes((int)(self.samplerate * self.dureeLecture))
			for index in range(len(frames)//self.samplewidth):
				sample = audioop.getsample(frames, self.samplewidth, index)
				yield sample/self.max_amplitude


# Retourne le bpm de l'échantillon.
def get_bpm(tableau, fs):
	
	d = len(tableau)
	listBpm = range(50,200,10)
	maximum = 0.0
	bpm = 0
	
	for b in listBpm:
		decalage = (int)(4 * 60*fs/b)
		correl = 0.
		for k in range(1,d-decalage):
			correl += (tableau[k]*tableau[k+decalage])/(d-decalage)
		if(correl >= maximum):
			bpm = b
			maximum = correl
	return bpm

# Fonction qui calcule le pitch-class à partir d'un tableau de données brutes

def pcp(tableau, fs):
		tab_fft = fft(tableau)
		d = len(tab_fft)
		
		f_ref = 130.80   # Fréquence de la première case. Ici c'est un do.
		PCP = list(range(0,12))
		m = 0
		sumPCP = 0
		
		#Calcul effectif du PCP
		for p in range(0,12):
			PCP[p] = 0
			for l in range(0,d):
				if l==0:
					m = -1
				else:
					m = round(12 * math.log(fs*l/d),2) % 12
				if m==p:
					PCP[p] += abs(tab_fft[l])*abs(tab_fft[l])
			sumPCP += PCP[p]
		# Normalisation
		if sumPCP != 0:		#Pour le cas où il y a de longs moments de silence.
			for p in range(0,12):
				PCP[p] /= sumPCP
		return PCP
		 

### Fonction écrivant un tableau horizontalement dans un fichier texte ###

def ecrireTableau(tableau, adresseEcriture):
	with open(adresseEcriture, 'a') as fichier:  # en mode ajout au contenu déjà existant.
		#fichier.write('[')
		l = len(tableau)
		for i in range(0, l-1):			# cette définition signifie jusqu'à l-2. Python is weird...
			fichier.write(str(tableau[i]))
			fichier.write(',')
		fichier.write(str(tableau[l-1]))
		#fichier.write(']')
		fichier.write('\n')

#tests
# ecrireTableau([0,1,2,3,4],"D:\PSC\Code\Python\PCP\pcp_2\ecriture.txt")
# ecrireTableau(pcp([0,1,2,3,4], 2), "D:\PSC\Code\Python\PCP\pcp_2\ecriture.txt")

# On prend la suite de PCP des 5 premières secondes du morceau donné.
# Les fenêtres sont de 0.1 secondes.
def extractPCP(adresseFichier, adresseEcriture):
	with WavFile(adresseFichier, 6) as fichier:
		a = list(fichier.get_samples())
		fs = fichier.samplerate
		d=len(a)
		
		# On extrait le bpm pour trouver la fenêtre
		fenetre = round(get_bpm(a,fs)/4)
		with open(adresseEcriture, 'a') as lieu:
			lieu.write(adresseFichier)		# On dit de quel extrait ça vient
			lieu.write('\n')
			
		### Ecrit dans le fichier à l'adresse donnée la suite des tableaux PCP du morceau.
		nb = (int)(d/fenetre)
		
		# On découpe ici en fenêtres déterminées par le rythme, qu'on considère comme étant celui de la noire.
		for i in range(0, nb):
			temp = a[i:i+fenetre-1]
			ecrireTableau(pcp(temp, fs), adresseEcriture)

extractPCP("D:\PSC\Echantillons\Elise (2).wav", "D:\PSC\Code\Python\PCP\pcp_2\ecriture.txt")
		
### Maintenant la même chose avec une liste d'adresses de fichier ###
def traiter_liste(liste, adresseEcriture):
	for i in range(0, len(liste)):
		extractPCP(liste[i], adresseEcriture)


### Et avec un fichier contenant les noms liste par liste ###
def traiter(adresseLecture, adresseEcriture):
	dossier = "D:/PSC/Echantillons/"		# là où sont les fichiers wav
	with open(adresseLecture, 'r') as lecture:
		for ligne in lecture:
			adresseAbsolue = dossier + ligne[:-1]    # pour enlever le \n à la fin
			print("debut traitement" + adresseAbsolue)
			extractPCP(adresseAbsolue, adresseEcriture)
			print("fin traitement" + adresseAbsolue)

#traiter("D:\PSC\Echantillons\liste.txt", "D:/PSC/Echantillons/resultats.txt")
		
